#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>

//  модификатор inline, чтобы при вызове тело функции подставлялось в месте вызова вместо вызова (удобно для небольших функций)
inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

_Noreturn void err( const char* msg, ... );


#endif
