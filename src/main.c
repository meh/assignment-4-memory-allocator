//
// Created by user on 3/13/24.
//

#include <stdio.h>

#include "mem.h"

#define REGION_SIZE 4096

void debug(const char *fmt, ...);

void test_allocate() {
    debug("Test access allocate memory... ");
    void *main_heap = heap_init(0);
    void *block1 = _malloc(2);
    void *block2 = _malloc(sizeof(size_t) * 100);
    void *block3 = _malloc(sizeof(int8_t));
    debug_heap(stderr, main_heap);
    _free(block1);
    _free(block2);
    _free(block3);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\n");
}

void test_free_one() {
    debug("Test free one block... ");
    void *main_heap = heap_init(0);
    _malloc(sizeof(uint16_t));
    void *block = _malloc(sizeof(uint16_t));
    _malloc(sizeof(uint16_t));
    debug_heap(stderr, main_heap);
    _free(block);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\n");
}

void test_free_two() {
    debug("Test free two blocks... ");
    debug("Case 1.");
    void *main_heap1 = heap_init(0);
    _malloc(sizeof(uint8_t));
    _malloc(sizeof(uint8_t));
    void *block1 = _malloc(sizeof(uint8_t));
    void *block2 = _malloc(sizeof(uint8_t));
    _malloc(sizeof(uint8_t));
    debug_heap(stderr, main_heap1);
    _free(block1);
    _free(block2);
    debug_heap(stderr, main_heap1);
    heap_term();

    debug("Case 2.");
    void *main_heap2 = heap_init(0);
    _malloc(sizeof(uint8_t));
    void *block3 = _malloc(sizeof(uint8_t));
    _malloc(sizeof(uint8_t));
    void *block4 = _malloc(sizeof(uint8_t));
    _malloc(sizeof(uint8_t));
    debug_heap(stderr, main_heap2);
    _free(block3);
    _free(block4);
    debug_heap(stderr, main_heap2);
    heap_term();
    debug("\n");
}

void test_grow_region() {
    debug("Test grow new region... ");
    void *main_heap = heap_init(0);
    void *block1 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void *block2 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void *block3 = _malloc(REGION_SIZE);
    _free(block1);
    debug_heap(stderr, main_heap);
    _free(block2);
    debug_heap(stderr, main_heap);
    _free(block3);
    debug_heap(stderr, main_heap);
    heap_term();
    debug("\n");
}

void test_new_region_in_another_place() {
    debug("Test new region grow in another place... ");
    void *main_heap = heap_init(0);
    void *blocked_block_addr = main_heap + REGION_SIZE;
    void* blocked_block = mmap(blocked_block_addr, REGION_SIZE, PROT_READ | PROT_WRITE,
                               MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *block1 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    void *block2 = _malloc(REGION_SIZE);
    debug_heap(stderr, main_heap);
    _free(block1);
    debug_heap(stderr, main_heap);
    _free(block2);
    debug_heap(stderr, main_heap);
    _free(blocked_block);
    heap_term();
    debug("\n");
}

int main() {
    test_allocate();
    test_free_one();
    test_free_two();
    test_grow_region();
    test_new_region_in_another_place();
    return 0;
}
