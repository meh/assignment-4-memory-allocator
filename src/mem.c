#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

//  модификатор restrict.
//  этим модификатором мы сообщаем компилятору, что указатель ссылается на область памяти, на которую не ссылаются другие указатели
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    //  сборка (наполнение) структуры block_header
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

//  size_max() из util.h выбирает больший из переданных аргументов типа size_t
//  round_pages() возвращает количество байтов на странице, умноженное на количество страниц (посчитанное pages_count())
static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
    //  void * mmap(void *start, size_t length, int prot , int flags, int fd, off_t offset);
    //  с адреса start отражает в памяти length байтов
    //  режим защиты памяти: данные можно читать и переписывать. тип отражаемого объекта. ???здесь и без смещения???
    //  возвращает местоположение отображенных данных (никогда не равно нулю)
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    //  из mem_internals.h:  struct region { void* addr; size_t size; bool extends; };
    //  регион -- большая область памяти под кучу
    bool extends = true;
    //  размер региона складывается из запрошенного объема и смещения contents[] внутри block_header
    //  макрос offsetof(type, member) возвращает смещение поля member внутри type
    //      block_header из mem_internals.h
    size_t size = region_actual_size(query + offsetof(struct block_header, contents));
    void *start_address = map_pages(addr, size, MAP_FIXED);
    if (start_address == MAP_FAILED) {
        extends = false;
        start_address = map_pages(addr, size, 0);
        if (start_address == MAP_FAILED) {
            return (struct region) REGION_INVALID;
        }
    }   //  пытаемся отобразить место для региона
    //  если успешно, инициализируем первый блок (пока единственный)
    block_init(start_address, (block_size) {.bytes = size}, NULL);
    struct region region = (struct region) {.extends = extends, .addr = start_address, .size = size};
    return region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
    //  создается неизменяемый регион. если регион валидный, функция возвращает ссылку на него. этот регион -- куча.
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
    //  получение указателя на блок после переданного блока
    return (void*) (block->contents + block->capacity.bytes);
}

//  наверное, можно было просто объявить функцию раньше, как в случае с block_after()
static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    //  блоки расположены один за другим, если указатель второго == block_after() от первого блока
    return (void *) snd == block_after(fst);
}

static bool mergeable(
        struct block_header const* restrict fst,
        struct block_header const* restrict snd) {
    //  проверка, свободны ли блоки и следуют ли друг за другом
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

//  -------------------------------------------------------
/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    //  block указывает на block_header с адресом HEAP_START
    struct block_header *block = (struct block_header*) HEAP_START;
    void *clean_address = block;
    struct block_header *addr = block;
    //  пока не конец списка блоков
    while (addr != NULL) {
        size_t length = 0;
        //  пока блоки следуют друг за другом
        while (blocks_continuous(addr, addr->next)) {
            //  длина наращивается за счет размеров блоков (вместимость хранится в структуре, которая хранится в блоке)
            length += size_from_capacity(addr->capacity).bytes;
            //  берется адрес следующего блока
            addr = addr->next;
        }
        //  для последнего блока указатель NULL, но его размер еще не посчитан
        length += size_from_capacity(addr->capacity).bytes;
        addr = addr->next;
        //  munmap(address, length)
        //  для удаления отображения объекта в памяти
        munmap(clean_address, length);
        clean_address = addr;
    }
}

#define BLOCK_MIN_CAPACITY 24


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    //  если блок пуст и (запрошенный объем + размер заголовка без контента + наименьшая вместимость блока) <= вместимости блока
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//  -------------------------------------------------------
static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block, query)) {
        struct block_header *next_block = block->next;
        void *first_block_address = block;
        void *second_block_address = query + block->contents;
        //  размер первого блока -- query + заголовок
        block_size first_block_size = {.bytes = size_from_capacity((block_capacity) {.bytes = query}).bytes};
        //  размер второго -- вместимость - query
        block_size second_block_size = {.bytes = block->capacity.bytes - query};
        block_init(first_block_address, first_block_size, second_block_address);
        block_init(second_block_address, second_block_size, next_block);
        return true;
    }
    return false;
}

//  -------------------------------------------------------
static bool try_merge_with_next( struct block_header* block ) {
    //  если блок не последний и соединяем
    if (block->next != NULL && mergeable(block, block->next)) {
        //  адрес блока через один
        struct block_header *block_next = block->next->next;
        //  новый размер блока -- (два заголовка + две вместимости)
        block_size size = {.bytes = 2 * offsetof(struct block_header, contents) + block->capacity.bytes + block->next->capacity.bytes};
        block_init(block, size, block_next);
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    //  структура для возвращения результата
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


//  -------------------------------------------------------
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    //  значения по умолчанию
    struct block_search_result bsr = {.type = BSR_CORRUPTED};
    struct block_header *cur_block = block;
    struct block_header *prev_block = NULL;
    bool is_corrupted = true;
    //  пока указатель существует
    while (cur_block != NULL) {
        is_corrupted = false;
        //  мержим, пока мержится
        while(try_merge_with_next(cur_block)){};
        //  если вместимость больше, чем требуемый размер, и свободен
        if (cur_block->capacity.bytes >= sz && cur_block->is_free) {
            bsr.type = BSR_FOUND_GOOD_BLOCK;
            bsr.block = cur_block;
            return bsr;
        }
        //  следующий блок
        prev_block = cur_block;
        cur_block = cur_block->next;
    }
    //  если на блок нет указателя и блок не кораптед
    if (!cur_block && !is_corrupted) {
        bsr.block = prev_block;
        bsr.type = BSR_REACHED_END_NOT_FOUND;
    }
    return bsr;
}

//  -------------------------------------------------------
/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    //  ищем хороший (вместимость >= query) блок, начиная с block
    struct block_search_result bsr = find_good_or_last(block, query);
    //  если блок найден
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        //  делим, если слишком большой
        split_if_too_big(bsr.block, query);
        //  занимаем блок
        bsr.block->is_free = false;
    }
    return bsr;
}


//  -------------------------------------------------------
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    //  идем от contents на величину вместимости, аллоцируем query байт
    struct region region = alloc_region(last->contents + last->capacity.bytes, query);
    if(region_is_invalid(&region)){
        return NULL;
    }
    //  следующий блок в новом регионе
    last->next = region.addr;
    try_merge_with_next(last);
    struct block_header *block = last;
    //  регион не расширяем или последний блок не свободен
    if (!region.extends || !last->is_free) {
        block = region.addr;
    }
    return block;
}

//  -------------------------------------------------------
/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);
    switch (bsr.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return bsr.block;
        case BSR_CORRUPTED:
            return NULL;
        case BSR_REACHED_END_NOT_FOUND: {
            struct block_header *block = grow_heap(bsr.block, query);
            bsr = try_memalloc_existing(query, block);
        }
    }
    return bsr.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    //  -------------------------------------------------------
    try_merge_with_next(header);
}
