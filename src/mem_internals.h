#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

#define REGION_MIN_SIZE (2 * 4096)

struct region { void* addr; size_t size; bool extends; };
static const struct region REGION_INVALID = {0};

inline bool region_is_invalid( const struct region* r ) { return r->addr == NULL; }

//  псевдоним для структуры, видимый в глобальном контексте
//  typedef struct point_t Point;
//  point_t p = {10, 20};
//  Point px = {15, -4};

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

struct block_header {
  struct block_header*    next; //  указатель на следующий элемент списка
  block_capacity capacity;  //  вместимость -- размер блока без заголовка
  bool           is_free;   //  свободен ли
  uint8_t        contents[];    //  сами данные (кстати, flexible array. Массив с не определенной заранее величиной)
};

inline block_size size_from_capacity( block_capacity cap ) { return (block_size) {cap.bytes + offsetof( struct block_header, contents ) }; }
inline block_capacity capacity_from_size( block_size sz ) { return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) }; }

#endif
